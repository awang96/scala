package projetal2020.models.mower

import projetal2020.models.Point

case class Stand(point: Point, direction: String) {}
