package projetal2020.models.mower

case class MowerResult(
    initial: Stand,
    instructions: List[String],
    end: Stand
)
