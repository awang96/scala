package projetal2020.models.mower

import projetal2020.models.Point
import projetal2020.models.garden.Garden
import projetal2020.utils.InstructionImpossibleException

import scala.util.{Failure, Success, Try}

case class Mower(
    initial: Stand,
    instructions: List[String]
) {
  def mow(garden: Garden): Try[Stand] = {
    def helper(stand: Stand, instructions: List[String]): Try[Stand] =
      instructions match {
        case "A" :: tail =>
          stand.direction match {
            case "N" =>
              if (stand.point.y + 1 > garden.limit.y) {
                helper(stand, tail)
              } else {
                helper(
                  Stand(
                    Point(
                      stand.point.x,
                      stand.point.y + 1
                    ),
                    stand.direction
                  ),
                  tail
                )
              }
            case "E" =>
              if (stand.point.x + 1 > garden.limit.x) {
                helper(stand, tail)
              } else {
                helper(
                  Stand(
                    Point(
                      stand.point.x + 1,
                      stand.point.y
                    ),
                    stand.direction
                  ),
                  tail
                )
              }
            case "W" =>
              if (stand.point.x - 1 < 0) {
                helper(stand, tail)
              } else {
                helper(
                  Stand(
                    Point(
                      stand.point.x - 1,
                      stand.point.y
                    ),
                    stand.direction
                  ),
                  tail
                )
              }
            case "S" =>
              if (stand.point.y - 1 < 0) {
                helper(stand, tail)
              } else {
                helper(
                  Stand(
                    Point(
                      stand.point.x,
                      stand.point.y - 1
                    ),
                    stand.direction
                  ),
                  tail
                )
              }
          }
        case "D" :: tail =>
          stand.direction match {
            case "N" =>
              helper(
                Stand(
                  stand.point,
                  "E"
                ),
                tail
              )
            case "E" =>
              helper(
                Stand(
                  stand.point,
                  "S"
                ),
                tail
              )
            case "W" =>
              helper(
                Stand(
                  stand.point,
                  "N"
                ),
                tail
              )
            case "S" =>
              helper(
                Stand(
                  stand.point,
                  "W"
                ),
                tail
              )
          }
        case "G" :: tail =>
          stand.direction match {
            case "N" =>
              helper(
                Stand(
                  stand.point,
                  "W"
                ),
                tail
              )
            case "E" =>
              helper(
                Stand(
                  stand.point,
                  "N"
                ),
                tail
              )
            case "W" =>
              helper(
                Stand(
                  stand.point,
                  "S"
                ),
                tail
              )
            case "S" =>
              helper(
                Stand(
                  stand.point,
                  "E"
                ),
                tail
              )
          }
        case Nil => Success(stand)
        case _ =>
          Failure(
            InstructionImpossibleException(
              "Une instruction impossible a ete trouvee"
            )
          )
      }

    helper(this.initial, this.instructions)
  }
}
