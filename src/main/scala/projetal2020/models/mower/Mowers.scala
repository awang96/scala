package projetal2020.models.mower

import projetal2020.models.garden.Garden

import scala.util.{Failure, Success, Try}

object Mowers {
  def mow(mowers: List[Mower], garden: Garden): Try[List[MowerResult]] =
    mowers match {
      case head :: tail =>
        head.mow(garden) match {
          case Success(end) =>
            val res = MowerResult(
              head.initial,
              head.instructions,
              end
            )
            val rest = mow(tail, garden)
            rest match {
              case Success(mowers)    => Success(res :: mowers)
              case Failure(exception) => Failure(exception)
            }
          case Failure(exception) => Failure(exception)
        }
      case Nil => Success(Nil)
    }
}
