package projetal2020.utils

case class DonneesIncorrectesException(msg: String) extends Throwable(msg)
