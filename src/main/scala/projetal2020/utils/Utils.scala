package projetal2020.utils

import better.files.File
import play.api.libs.json.{JsObject, JsValue, Json, Writes}
import projetal2020.models.Point
import projetal2020.models.garden.Garden
import projetal2020.models.mower.{Mower, MowerResult, Stand}

import scala.util.{Failure, Success, Try}

object Utils {
  def readArgs(
      args: Array[String]
  ): Try[Tuple2[Garden, List[Mower]]] =
    args.length match {
      case 2 => {
        val fileName = args(0)
        readFile(fileName) match {
          case Success(value) => Success(value)
          case Failure(e)     => Failure(e)
        }
      }
      case _ =>
        Failure(
          DonneesIncorrectesException(
            "Fichier d'entree ou de sortie non indique"
          )
        )
    }

  def readFile(fileName: String): Try[Tuple2[Garden, List[Mower]]] = {
    val file = File(fileName)
    val content = Try(file.lines.toList)
    content match {
      case Success(content) => {
        getGarden(content(0)) match {
          case Success(garden) => {
            getMowers(content.drop(1), garden) match {
              case Success(mowers) => {
                Success(
                  Tuple2(
                    garden,
                    mowers
                  )
                )
              }
              case Failure(e) => Failure(e)
            }
          }
          case Failure(e) => Failure(e)
        }
      }
      case Failure(nsf) => Failure(nsf)
    }
  }

  def getGarden(content: String): Try[Garden] = {
    val coords = content.split(" ")
    coords.length match {
      case 2 => {
        try {
          val x = coords(0).toInt
          val y = coords(1).toInt
          if (x <= 0 || y <= 0) {
            Failure(
              DonneesIncorrectesException(
                "Une ou plusieurs coordonnees sont inferieurs a 0"
              )
            )
          } else {
            Success(
              Garden(Point(x, y))
            )
          }
        } catch {
          case _: NumberFormatException =>
            Failure(
              DonneesIncorrectesException(
                "Une ou plusieurs coordonnees ne sont pas numeriques"
              )
            )
        }
      }
      case _ =>
        Failure(
          DonneesIncorrectesException(
            s"Des coordonnees pour le jardin sont absentes"
          )
        )
    }
  }

  def getMowers(content: List[String], garden: Garden): Try[List[Mower]] = {
    def getStartingPoint(line: String, count: Int): Try[Stand] = {
      val splitLine = line.split(" ")
      splitLine.length match {
        case 3 => {
          try {
            val x = splitLine(0).toInt
            val y = splitLine(1).toInt
            val direction = splitLine(2)
            if (x < 0 || x > garden.limit.x || y < 0 || y > garden.limit.y) {
              Failure(
                DonneesIncorrectesException(
                  s"Des coordonnees de la tondeuse ${count.toString} sont incorrectes"
                )
              )
            } else {
              if (List("N", "E", "W", "S").contains(direction)) {
                Success(
                  Stand(
                    Point(x, y),
                    direction
                  )
                )
              } else {
                Failure(
                  DonneesIncorrectesException(
                    s"La direction pour la tondeuse ${count.toString} est incorrecte"
                  )
                )
              }
            }
          } catch {
            case _: NumberFormatException =>
              Failure(
                DonneesIncorrectesException(
                  s"Des coordonnees pour la tondeuse ${count.toString} ne sont pas numeriques"
                )
              )
          }
        }
        case _ =>
          Failure(
            DonneesIncorrectesException(
              s"Des coordonnees pour la tondeuse ${count.toString} sont manquantes"
            )
          )
      }
    }

    def helper(value: List[String], count: Int): Try[List[Mower]] =
      value match {
        case first :: second :: tail => {
          val start = getStartingPoint(first, count)
          start match {
            case Success(stand) => {
              second.length match {
                case 0 =>
                  Failure(
                    DonneesIncorrectesException(
                      s"Aucune instruction n'a ete trouvee pour la tondeuse ${count.toString}"
                    )
                  )
                case _ => {
                  second.filter(!List('A', 'G', 'D').contains(_)).length match {
                    case 0 => {
                      val mower = Mower(
                        stand,
                        second.toList.map(_.toString)
                      )
                      val rest = helper(tail, count + 1)
                      rest match {
                        case Success(mowers) => Success(mower :: mowers)
                        case Failure(e)      => Failure(e)
                      }
                    }
                    case _ =>
                      Failure(
                        DonneesIncorrectesException(
                          s"Des instructions incorrectes ont ete trouvees pour la tondeuse ${count.toString}"
                        )
                      )
                  }
                }
              }
            }
            case Failure(e) => Failure(e)
          }
        }
        case Nil => Success(Nil)
        case _ =>
          Failure(
            DonneesIncorrectesException(
              s"Des donnees manquent pour la tondeuse ${count.toString}"
            )
          )
      }

    helper(content, 1) match {
      case Success(value) => Success(value)
      case Failure(e)     => Failure(e)
    }
  }

  def toJson(mowers: List[MowerResult], garden: Garden): JsValue = {
    implicit val pointWrite = new Writes[Point] {
      def writes(point: Point): JsObject = Json.obj(
        "x" -> point.x,
        "y" -> point.y
      )
    }
    implicit val standWrite = new Writes[Stand] {
      def writes(stand: Stand): JsObject = Json.obj(
        "point"     -> stand.point,
        "direction" -> stand.direction
      )
    }
    implicit val mowerResultWrite = new Writes[MowerResult] {
      def writes(mowerResult: MowerResult): JsObject = Json.obj(
        "initial"      -> mowerResult.initial,
        "instructions" -> mowerResult.instructions,
        "end"          -> mowerResult.end
      )
    }
    implicit val gardenWrite = new Writes[Garden] {
      def writes(garden: Garden): JsObject = Json.obj(
        "limit" -> garden.limit
      )
    }
    implicit val gardenMowerTupleWriter =
      new Writes[Tuple2[Garden, List[MowerResult]]] {
        def writes(tuple: Tuple2[Garden, List[MowerResult]]): JsObject =
          Json.obj(
            "garden" -> tuple._1,
            "mowers" -> tuple._2
          )
      }
    Json.toJson((garden, mowers))
  }

  def writeOutput(
      fileName: String,
      mowers: List[MowerResult],
      garden: Garden
  ): Unit = {
    val output = File(fileName)
    output
      .createIfNotExists()
      .overwrite(Json.prettyPrint(toJson(mowers, garden)))
    println("Fichier cree")
  }
}
