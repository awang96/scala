package projetal2020.utils

case class InstructionImpossibleException(msg: String) extends Throwable(msg)
