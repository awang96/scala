package projetal2020

import projetal2020.models.mower.Mowers
import projetal2020.utils.Utils

import scala.util.{Failure, Success}

object Main extends App {
  println("Ici le programme principal")
  Utils.readArgs(args) match {
    case Success(value) =>
      val (garden, mowers) = value
      Mowers.mow(mowers, garden) match {
        case Success(value)     => Utils.writeOutput(args(1), value, garden)
        case Failure(exception) => println(exception)
      }
    case Failure(exception) => println(exception)
  }
}
