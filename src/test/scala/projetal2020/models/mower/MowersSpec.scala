package projetal2020.models.mower

import org.mockito.Mockito.{spy, when}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.mockito.MockitoSugar
import projetal2020.models.Point
import projetal2020.models.garden.Garden
import projetal2020.utils.InstructionImpossibleException

import scala.util.{Failure, Success}

class MowersSpec extends AnyFunSuite with MockitoSugar {
  val garden = Garden(Point(5, 5))

  test("Should return Nil when Nil is passed") {
    assert(Mowers.mow(Nil, garden) === Success(Nil))
  }

  test("Should return mower with ending point") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "A" :: Nil
    )
    val spiedMower = spy(mower)
    val spiedRes = Stand(Point(3, 3), "N")
    val spiedMethod = when(spiedMower.mow(garden)).thenReturn(Success(spiedRes))
    val mowers = spiedMower :: Nil
    val res = MowerResult(
      mower.initial,
      mower.instructions,
      spiedRes
    ) :: Nil
    println(spiedMethod)
    assert(Mowers.mow(mowers, garden) === Success(res))
  }

  test("Should return Failure when mowing fails") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "A" :: Nil
    )
    val spiedMower = spy(mower)
    val spiedRes = Failure(
      InstructionImpossibleException("a")
    )
    val spiedMethod = when(spiedMower.mow(garden)).thenReturn(spiedRes)
    println(spiedMethod)
    assert(Mowers.mow(spiedMower :: Nil, garden) === spiedRes)
  }
}
