package projetal2020.models.mower

import org.scalatest.funsuite.AnyFunSuite
import projetal2020.models.Point
import projetal2020.models.garden.Garden
import projetal2020.utils.InstructionImpossibleException

import scala.util.{Failure, Success}

class MowerSpec extends AnyFunSuite {
  val garden = Garden(Point(5, 5))

  test("Mower should move North") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "A" :: Nil
    )
    val res = Success(Stand(Point(0, 1), "N"))
    assert(mower.mow(this.garden) === res)
  }

  test("Mower shouldn't move North") {
    val mower = Mower(
      Stand(
        Point(5, 5),
        "N"
      ),
      "A" :: Nil
    )
    assert(mower.mow(garden) === Success(mower.initial))
  }

  test("Mower should move East") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "E"
      ),
      "A" :: Nil
    )
    val res = Success(Stand(Point(1, 0), "E"))
    assert(mower.mow(this.garden) === res)
  }

  test("Mower shouldn't move East") {
    val mower = Mower(
      Stand(
        Point(5, 5),
        "E"
      ),
      "A" :: Nil
    )
    assert(mower.mow(garden) === Success(mower.initial))
  }

  test("Mower should move West") {
    val mower = Mower(
      Stand(
        Point(5, 5),
        "W"
      ),
      "A" :: Nil
    )
    val res = Success(Stand(Point(4, 5), "W"))
    assert(mower.mow(garden) === res)
  }

  test("Mower shouldn't move West") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "W"
      ),
      "A" :: Nil
    )
    assert(mower.mow(garden) === Success(mower.initial))
  }

  test("Mower should move South") {
    val mower = Mower(
      Stand(
        Point(5, 5),
        "S"
      ),
      "A" :: Nil
    )
    val res = Success(Stand(Point(5, 4), "S"))
    assert(mower.mow(garden) === res)
  }

  test("Mower shouldn't move South") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "S"
      ),
      "A" :: Nil
    )
    assert(mower.mow(garden) === Success(mower.initial))
  }

  test("Mower should turn from North to East") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "D" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "E"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from East to South") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "E"
      ),
      "D" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "S"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from South to West") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "S"
      ),
      "D" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "W"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from West to North") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "W"
      ),
      "D" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "N"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from North to West") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "G" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "W"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from West to South") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "W"
      ),
      "G" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "S"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from South to East") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "S"
      ),
      "G" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "E"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should turn from East to North") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "E"
      ),
      "G" :: Nil
    )
    val res = Success(Stand(Point(0, 0), "N"))
    assert(mower.mow(garden) === res)
  }

  test("Mower should stay still when no instructions are issued") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      Nil
    )
    assert(mower.mow(garden) === Success(mower.initial))
  }

  test("Mower should return Exception when invalid instructions is issued") {
    val mower = Mower(
      Stand(
        Point(0, 0),
        "N"
      ),
      "S" :: Nil
    )
    val exception = Failure(
      InstructionImpossibleException(
        "Une instruction impossible a ete trouvee"
      )
    )
    assert(mower.mow(garden) === exception)
  }
}
