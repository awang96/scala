package projetal2020.utils

import org.scalatest.funsuite.AnyFunSuite

import scala.util.Failure

class UtilsSpec extends AnyFunSuite {
  test("readArgs should return Failure when there aren't two arguments") {
    val res = Failure(
      DonneesIncorrectesException("Fichier d'entree ou de sortie non indique")
    )
    assert(Utils.readArgs(Array()) === res)
  }
}
